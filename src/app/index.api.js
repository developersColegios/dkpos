(function () {
    'use strict';

    angular
        .module('fuse')
        .factory('api', apiService);

    /** @ngInject */
    function apiService($resource) {
        var api = {};

        // Base Url
        api.baseUrl = 'http://127.0.0.1:8080/api/';

        api.DkPOSWS = {
            Auth: {
                login: $resource(api.baseUrl + 'Auth/login/:val', { val: '@_val' }, {
                    send: {
                        method: 'POST'
                    }
                }),
                logout: $resource(api.baseUrl + 'Auth/logout/:val', { val: '@_val' }, {
                    send: {
                        method: 'POST'
                    }
                }),
                me: $resource(api.baseUrl + 'Auth/verify/:val', { val: '@_val' })
            },
            User: $resource(api.baseUrl + 'User/:id', { id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                }
            }),
            CustomerSearch: $resource(api.baseUrl + 'Customer/:nit/nit', { nit: '@_nit' }),
            Customer: $resource(api.baseUrl + 'Customer/:id', { id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                },
                delete: {
                    method: 'DELETE'
                }
            }),
            ProviderSearch: $resource(api.baseUrl + 'Provider/:nit/nit', { nit: '@_nit' }),
            Provider: $resource(api.baseUrl + 'Provider/:id', { id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                },
                delete: {
                    method: 'DELETE'
                }
            }),
            Product: $resource(api.baseUrl + 'Product/:id', { id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                },
                delete: {
                    method: 'DELETE'
                }
            }),
            Kardex: $resource(api.baseUrl + 'Kardex'),
            InvoiceLast: $resource(api.baseUrl + 'Invoice/pendent'),
            Invoice: $resource(api.baseUrl + 'Invoice/:id', { id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                },
                delete: {
                    method: 'DELETE'
                }
            }),
            InvoiceDetail: $resource(api.baseUrl + 'Invoice/:idInvoice/:id', { idInvoice: '@_idInvoice', id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                },
                delete: {
                    method: 'DELETE'
                }
            }),
            BuyLast: $resource(api.baseUrl + 'Buy/pendent'),
            Buy: $resource(api.baseUrl + 'Buy/:id', { id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                },
                delete: {
                    method: 'DELETE'
                }
            }),
            BuyDetail: $resource(api.baseUrl + 'Buy/:idBuy/:id', { idBuy: '@_idBuy', id: '@_id' }, {
                update: {
                    method: 'PUT'
                },
                patch: {
                    method: 'PATCH'
                },
                save: {
                    method: 'POST',
                },
                delete: {
                    method: 'DELETE'
                }
            })
        };

        return api;
    }

})();