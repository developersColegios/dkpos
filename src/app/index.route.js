(function () {
    'use strict';

    angular
        .module('fuse')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/login');

        /**
         * Layout Style Switcher
         *
         * This code is here for demonstration purposes.
         * If you don't need to switch between the layout
         * styles like in the demo, you can set one manually by
         * typing the template urls into the `State definitions`
         * area and remove this code
         */
        // Inject $cookies
        var $cookies;

        angular.injector(['ngCookies']).invoke([
            '$cookies', function (_$cookies) {
                $cookies = _$cookies;
            }
        ]);

        // Get active layout
        var layoutStyle = $cookies.get('layoutStyle') || 'verticalNavigationFullwidthToolbar2';

        var layouts = {
            verticalNavigation: {
                main: 'app/core/layouts/vertical-navigation.html',
                toolbar: 'app/toolbar/layouts/vertical-navigation/toolbar.html',
                navigation: 'app/navigation/layouts/vertical-navigation/navigation.html'
            },
            verticalNavigationFullwidthToolbar: {
                main: 'app/core/layouts/vertical-navigation-fullwidth-toolbar.html',
                toolbar: 'app/toolbar/layouts/vertical-navigation-fullwidth-toolbar/toolbar.html',
                navigation: 'app/navigation/layouts/vertical-navigation/navigation.html'
            },
            verticalNavigationFullwidthToolbar2: {
                main: 'app/core/layouts/vertical-navigation-fullwidth-toolbar-2.html',
                toolbar: 'app/toolbar/layouts/vertical-navigation-fullwidth-toolbar-2/toolbar.html',
                navigation: 'app/navigation/layouts/vertical-navigation-fullwidth-toolbar-2/navigation.html'
            },
            horizontalNavigation: {
                main: 'app/core/layouts/horizontal-navigation.html',
                toolbar: 'app/toolbar/layouts/horizontal-navigation/toolbar.html',
                navigation: 'app/navigation/layouts/horizontal-navigation/navigation.html'
            },
            contentOnly: {
                main: 'app/core/layouts/content-only.html',
                toolbar: '',
                navigation: ''
            },
            contentWithToolbar: {
                main: 'app/core/layouts/content-with-toolbar.html',
                toolbar: 'app/toolbar/layouts/content-with-toolbar/toolbar.html',
                navigation: ''
            }
        };
        // END - Layout Style Switcher

        // State definitions
        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    'main@': {
                        templateUrl: layouts[layoutStyle].main,
                        controller: 'MainController as vm'
                    },
                    'toolbar@app': {
                        templateUrl: layouts[layoutStyle].toolbar,
                        controller: 'ToolbarController as vm'
                    },
                    'navigation@app': {
                        templateUrl: layouts[layoutStyle].navigation,
                        controller: 'NavigationController as vm'
                    },
                }, resolve: {
                    redirectIfNotAuthenticated: ['$q', '$state', '$timeout', '$rootScope', 'api', 'localStorageService', redirectIfNotAuthenticated]
                }
            })
            .state('auth', {
                abstract: true,
                views: {
                    'main@': {
                        templateUrl: 'app/core/layouts/content-only.html',
                        controller: 'AuthController as vm'
                    }
                }, resolve: {
                    skipIfAuthenticated: ['$q', '$state', '$timeout', '$rootScope', 'api', 'localStorageService', skipIfAuthenticated]
                }
            });

        function skipIfAuthenticated($q, $state, $timeout, $rootScope, api, localStorageService) {
            var defer = $q.defer();
            var tk = localStorageService.get('tkposdk');
            if (tk) {
                api.DkPOSWS.Auth.me.get({ 'val': tk },
                    function (response) {
                        $rootScope.account = response;
                        localStorageService.set('account', $rootScope.account);
                        if ($state.current.name === '' || $state.current.name === 'login') {
                            $timeout(function () {
                                $state.go('app.dashboards_project');
                            }, 200);
                            defer.resolve();
                        } else {
                            defer.reject();
                        }
                    },
                    function (response) {
                        localStorageService.remove('tkposdk');
                        defer.resolve();
                    }
                );
            } else {
                defer.resolve();
            }
            return defer.promise;
        }

        function redirectIfNotAuthenticated($q, $state, $timeout, $rootScope, api, localStorageService) {

            var defer = $q.defer();

            var tk = localStorageService.get('tkposdk');
            if (tk) {
                api.DkPOSWS.Auth.me.get({ 'val': tk },
                    function (response) {
                        $rootScope.account = response;
                        localStorageService.set('account', $rootScope.account);
                        defer.resolve();
                    },
                    function (response) {
                        console.error(response);
                        $timeout(function () {
                            $state.go('auth.login');
                            localStorageService.remove('tkposdk');
                        }, 200);
                        defer.reject();
                    }
                );
            } else {
                $timeout(function () {
                    $state.go('auth.login');
                }, 200);
                defer.reject();
            }
            return defer.promise;
        }
    }

})();