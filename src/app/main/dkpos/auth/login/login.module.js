(function () {
    'use strict';

    angular
        .module('auth.login', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('auth.login', {
                url: '/login',
                views: {
                    'content@auth': {
                        templateUrl: 'app/main/dkpos/auth/login/login.html',
                        controller: 'LoginController as vm'
                    }
                },
                bodyClass: 'login'
            });
    }

})();