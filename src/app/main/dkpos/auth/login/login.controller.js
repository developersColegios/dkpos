(function () {
    'use strict';

    angular
        .module('auth.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController(api, $state, $rootScope, localStorageService, $mdToast) {

        // Data
        var vm = this;
        console.log('En Login');
        // Methods
        vm.login = login;
        //////////

        function login(datas) {
            var promise = api.DkPOSWS.Auth.login.send({ val: datas.email, tk: datas.password }, {}).$promise;
            promise.then(
                function (resp) {
                    $rootScope.account = resp.user;
                    localStorageService.set('tkposdk', resp.token);
                    $state.go('app.dashboards_project');
                },
                function (err) {
                    toast('Datos Incorrectos');
                }
            );
        }

        function toast(msg) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(msg)
                    .position("top right")
                    .hideDelay(3000)
            );
        }
    }
})();