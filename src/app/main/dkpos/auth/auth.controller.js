(function ()
{
    'use strict';

    angular
            .module('fuse')
            .controller('AuthController', AuthController);

    /** @ngInject */
    function AuthController($scope, $rootScope)
    {
        // Data

        //////////

        // Remove the splash screen
        $scope.$on('$viewContentAnimationEnded', function (event)
        {
            if ( event.targetScope.$id === $scope.$id )
            {
                $rootScope.$broadcast('msSplashScreen::remove');
            }
        });
    }
})();

