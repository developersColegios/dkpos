(function () {
    'use strict';

    angular
        .module('app.invoice')
        .controller('InvoiceViewController', InvoiceViewController);

    /** @ngInject */
    function InvoiceViewController($scope, api, $state, $stateParams) {
        var vm = this;

        // Data
        vm.invoice = {};

        // Methods

        init();
        //////////

        function init() {
            api.DkPOSWS.Invoice.get({ 'id': $stateParams.id }, function (data) {
                vm.invoice = data;
                vm.customer = data.customer;
                api.DkPOSWS.InvoiceDetail.query({ 'idInvoice': vm.invoice.id, 'id': 'all' }, function (datas) {
                    vm.invoiceDetail = datas;
                });
            });
        }
    }
})();
