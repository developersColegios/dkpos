(function () {
    'use strict';

    angular
        .module('app.invoice')
        .controller('InvoiceEditController', InvoiceEditController);

    /** @ngInject */
    function InvoiceEditController($state, api) {
        var vm = this;

        // Data
        vm.invoice = {};
        vm.today = new Date();
        vm.invoiceDetail = [];
        vm.dtInstance = {};
        vm.mapdetails = {};
        vm.dtOptions = {
            dom: 'rt',
            columnDefs: [
                {
                    // Target the id column
                    targets: 0,
                    width: '72px'
                },
                {
                    // Target the image column
                    targets: 1,
                    filterable: false,
                    sortable: false,
                    width: '80px'
                },
                {
                    // Target the actions column
                    targets: 6,
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false
                }
            ],
            initComplete: function () {
                var api = this.api(),
                    searchBox = angular.element('body').find('#e-commerce-products-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function (event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            paging: false,
            scrollY: 'auto',
            responsive: true
        };

        vm.newStatus = '';

        // Methods
        vm.gotoOrders = gotoOrders;
        vm.searchCustomer = searchCustomer;
        vm.searchProduct = searchProduct;
        vm.changeDatas = changeDatas;
        vm.changeStatus = changeStatus;
        vm.deleteDetail = deleteDetail;
        init();
        //////////

        function init() {
            api.DkPOSWS.InvoiceLast.get({}, function (data) {
                vm.invoice = data;
                vm.customer = data.customer;
                api.DkPOSWS.InvoiceDetail.query({ 'idInvoice': vm.invoice.id, 'id': 'all' }, function (datas) {
                    vm.invoiceDetail = datas;
                    var i = 0;
                    for (var i = 0; i < datas.length; i++) {
                        vm.mapdetails[datas[i].id] = i;
                    }
                });
            });
        }

        function searchCustomer(keyEvent) {
            if (keyEvent.which === 13) {
                api.DkPOSWS.CustomerSearch.get({ 'nit': vm.customer.customerCode }, function (data) {
                    vm.invoice.customer = data;
                    var promise = api.DkPOSWS.Invoice
                        .save(vm.invoice).$promise;
                    promise.then(
                        function (data) {
                            vm.invoice = data;
                        },
                        function (er) {

                        }
                    );
                });
            }
        }

        function searchProduct(keyEvent) {
            if (keyEvent.which === 13) {
                updateAndSaveDetail(vm.pcodigo, {});
            }
        }

        function changeDatas(keyEvent, detail) {
            if (keyEvent.which === 13) {
                updateAndSaveDetail(detail.product.barCode, detail);
            }
        }

        function updateAndSaveDetail(code, detail) {
            var promise = api.DkPOSWS.InvoiceDetail
                .save({ 'idInvoice': vm.invoice.id, 'productCode': code }, detail).$promise;
            promise.then(
                function (data) {
                    if (vm.mapdetails[data.id] >= 0) {
                        vm.invoiceDetail[vm.mapdetails[data.id]] = data;
                    } else {
                        vm.mapdetails[data.id] = vm.invoiceDetail.length;
                        vm.invoiceDetail.push(data);
                    }
                    vm.pcodigo = '';
                },
                function (er) {
                    vm.pcodigo = '';
                }
            );
        }

        /**
         * Go to orders page
         */
        function gotoOrders() {
            $state.go('app.e-commerce.orders');
        }

        /**
         * Go to product page
         * @param id
         */
        function deleteDetail(id) {
            api.DkPOSWS.InvoiceDetail
                .delete({ 'idInvoice': vm.invoice.id, 'id': id },
                    function (data) {
                        vm.invoiceDetail.splice(vm.mapdetails[id], 1);
                        delete (vm.mapdetails[id]);
                    },
                    function (er) {

                    }
                );
        }

        function changeStatus() {
            api.DkPOSWS.Invoice
                .patch({ 'id': vm.invoice.id }, {},
                    function (data) {
                        $state.go('app.invoice.view', { id: vm.invoice.id });
                    },
                    function (er) {

                    }
                );
        }
    }
})();