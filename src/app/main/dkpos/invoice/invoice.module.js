(function () {
    'use strict';

    angular
        .module('app.invoice', [
            'wipImageZoom',
            'datatables',
            'textAngular',
            'xeditable'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.invoice', {
                abstract: true,
                url: '/invoice'
            })
            .state('app.kardex', {
                url: '/kardex',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/invoice/kardex/kardex.html',
                        controller: 'KardexController as vm'
                    }
                },
                bodyClass: 'ecommerce'
            })
            .state('app.invoice.invoice', {
                url: '/invoices',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/invoice/invoice.html',
                        controller: 'InvoiceController as vm'
                    }
                },
                bodyClass: 'ecommerce'
            })
            .state('app.invoice.new', {
                url: '/new',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/invoice/invoice-edit/invoice-edit.html',
                        controller: 'InvoiceEditController as vm'
                    }
                }
            })
            .state('app.invoice.view', {
                url: '/view/:id',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/invoice/invoice-view/invoice-view.html',
                        controller: 'InvoiceViewController as vm'
                    }
                },
                bodyClass: 'invoice printable'
            });


        // Navigation
        msNavigationServiceProvider.saveItem('apps.invoice', {
            title: 'Facturación',
            icon: 'icon-receipt',
            weight: 4
        });
        msNavigationServiceProvider.saveItem('apps.invoice.invoiceedit', {
            title: 'Crear Factura',
            state: 'app.invoice.new',
            icon: 'icon-cart-outline',
            weight: 2
        });
        msNavigationServiceProvider.saveItem('apps.invoice.invoices', {
            title: 'Facturas',
            state: 'app.invoice.invoice',
            icon: 'icon-receipt',
            weight: 3
        });
        msNavigationServiceProvider.saveItem('apps.invoice.kardex', {
            title: 'Kardex',
            state: 'app.kardex',
            icon: 'icon-code-equal',
            weight: 4
        });
    }

})();