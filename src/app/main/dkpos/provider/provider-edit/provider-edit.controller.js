(function () {
    'use strict';

    angular
        .module('app.provider')
        .controller('ProviderEditController', ProviderEditController);

    /** @ngInject */
    function ProviderEditController($scope, $state, api, $stateParams) {
        var vm = this;

        // Data
        vm.proveedor = {};

        // Methods
        vm.saveProvider = saveProvider;
        vm.gotoProviders = gotoProviders;
        vm.isFormValid = isFormValid;

        //////////

        init();

        /**
         * Initialize
         */
        function init() {
            if ($stateParams.id !== 0) {
                api.DkPOSWS.Provider.get({ 'id': $stateParams.id }, function (data) {
                    vm.proveedor = data;
                });
            }
        }

        /**
         * Save product
         */
        function saveProvider() {
            var promise;
            if (vm.proveedor.id) {
                promise = api.DkPOSWS.Provider
                    .update({ id: vm.proveedor.id }, vm.proveedor).$promise;
            } else {
                promise = api.DkPOSWS.Provider
                    .save(vm.proveedor).$promise;
            }

            promise.then(
                function (data) {
                    vm.proveedor = data;
                    $state.go('app.provider.provider');
                },
                function (er) {
                    
                }
            );

        }

        /**
         * Go to products page
         */
        function gotoProviders() {
            $state.go('app.provider.provider');
        }

        /**
         * Checks if the given form valid
         *
         * @param formName
         */
        function isFormValid(formName) {
            if ($scope[formName] && $scope[formName].$valid) {
                return $scope[formName].$valid;
            }
        }
    }
})();