(function () {
    'use strict';

    angular
        .module('app.provider', [
            'wipImageZoom',
            'datatables',
            'textAngular',
            'xeditable'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.provider', {
                abstract: true,
                url: '/provider'
            })
            .state('app.provider.provider', {
                url: '/',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/provider/provider.html',
                        controller: 'ProviderController as vm'
                    }
                },
                bodyClass: 'ecommerce'
            })
            .state('app.provider.provider.edit', {
                url: ':id',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/provider/provider-edit/provider-edit.html',
                        controller: 'ProviderEditController as vm'
                    }
                },
                bodyClass: 'e-commerce'
            });


        // Navigation
        msNavigationServiceProvider.saveItem('apps.provider', {
            title: 'Proveedores',
            state: 'app.provider.provider',
            icon: 'icon-people',
            weight: 2
        });
    }

})();