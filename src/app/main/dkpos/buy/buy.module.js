(function () {
    'use strict';

    angular
        .module('app.buy', [
            'wipImageZoom',
            'datatables',
            'textAngular',
            'xeditable'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.buy', {
                abstract: true,
                url: '/buy'
            })
            .state('app.buy.buy', {
                url: '/buys',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/buy/buy.html',
                        controller: 'BuyController as vm'
                    }
                },
                bodyClass: 'ecommerce'
            })
            .state('app.buy.new', {
                url: '/new',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/buy/buy-edit/buy-edit.html',
                        controller: 'BuyEditController as vm'
                    }
                }
            })
            .state('app.buy.view', {
                url: '/view/:id',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/buy/buy-view/buy-view.html',
                        controller: 'BuyViewController as vm'
                    }
                },
                bodyClass: 'invoice printable'
            });


        // Navigation
        msNavigationServiceProvider.saveItem('apps.buy', {
            title: 'Compras',
            icon: 'icon-receipt',
            weight: 4
        });
        msNavigationServiceProvider.saveItem('apps.buy.buyedit', {
            title: 'Crear Compra',
            state: 'app.buy.new',
            icon: 'icon-cart-outline',
            weight: 2
        });
        msNavigationServiceProvider.saveItem('apps.buy.buys', {
            title: 'Compras',
            state: 'app.buy.buy',
            icon: 'icon-receipt',
            weight: 3
        });
    }

})();