(function () {
    'use strict';

    angular
        .module('app.buy')
        .controller('BuyController', BuyController);

    /** @ngInject */
    function BuyController($state, api) {
        var vm = this;

        // Data
        vm.orders = [];

        vm.dtInstance = {};
        vm.dtOptions = {
            dom: 'rt',
            columnDefs: [
                {
                    // Target the id column
                    targets: 0,
                    width: '72px'
                },
                {
                    // Target the actions column
                    targets: 4,
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false
                }
            ],
            initComplete: function () {
                var api = this.api(),
                    searchBox = angular.element('body').find('#e-commerce-products-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function (event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            pagingType: 'simple',
            pageLength: 2000,
            scrollY: 'auto',
            responsive: true
        };

        // Methods
        vm.gotoOrderDetail = gotoOrderDetail;
        init();
        //////////

        function init() {
            api.DkPOSWS.Buy.query({}, function (data) {
                vm.buys = data;
            });
        }

        /**
         * Go to product detail
         *
         * @param id
         */
        function gotoOrderDetail(id) {
            $state.go('app.buy.view', { id: id });
        }
    }
})();