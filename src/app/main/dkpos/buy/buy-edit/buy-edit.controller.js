(function () {
    'use strict';

    angular
        .module('app.buy')
        .controller('BuyEditController', BuyEditController);

    /** @ngInject */
    function BuyEditController($state, api) {
        var vm = this;

        // Data
        vm.buy = {};
        vm.today = new Date();
        vm.buyDetail = [];
        vm.dtInstance = {};
        vm.mapdetails = {};
        vm.dtOptions = {
            dom: 'rt',
            columnDefs: [
                {
                    // Target the id column
                    targets: 0,
                    width: '72px'
                },
                {
                    // Target the image column
                    targets: 1,
                    filterable: false,
                    sortable: false,
                    width: '80px'
                },
                {
                    // Target the actions column
                    targets: 6,
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false
                }
            ],
            initComplete: function () {
                var api = this.api(),
                    searchBox = angular.element('body').find('#e-commerce-products-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function (event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            paging: false,
            scrollY: 'auto',
            responsive: true
        };

        vm.newStatus = '';

        // Methods
        vm.gotoOrders = gotoOrders;
        vm.searchProvider = searchProvider;
        vm.searchProduct = searchProduct;
        vm.changeDatas = changeDatas;
        vm.changeStatus = changeStatus;
        vm.deleteDetail = deleteDetail;
        init();
        //////////

        function init() {
            api.DkPOSWS.BuyLast.get({}, function (data) {
                vm.buy = data;
                vm.provider = data.provider;
                api.DkPOSWS.BuyDetail.query({ 'idBuy': vm.buy.id, 'id': 'all' }, function (datas) {
                    vm.buyDetail = datas;
                    var i = 0;
                    for (var i = 0; i < datas.length; i++) {
                        vm.mapdetails[datas[i].id] = i;
                    }
                });
            });
        }

        function searchProvider(keyEvent) {
            if (keyEvent.which === 13) {
                api.DkPOSWS.ProviderSearch.get({ 'nit': vm.provider.providerCode }, function (data) {
                    vm.buy.provider = data;
                    var promise = api.DkPOSWS.Buy
                        .save(vm.buy).$promise;
                    promise.then(
                        function (data) {
                            vm.buy = data;
                        },
                        function (er) {

                        }
                    );
                });
            }
        }

        function searchProduct(keyEvent) {
            if (keyEvent.which === 13) {
                updateAndSaveDetail(vm.pcodigo, {});
            }
        }

        function changeDatas(keyEvent, detail) {
            if (keyEvent.which === 13) {
                updateAndSaveDetail(detail.product.barCode, detail);
            }
        }

        function updateAndSaveDetail(code, detail) {
            var promise = api.DkPOSWS.BuyDetail
                .save({ 'idBuy': vm.buy.id, 'productCode': code }, detail).$promise;
            promise.then(
                function (data) {
                    if (vm.mapdetails[data.id] >= 0) {
                        vm.buyDetail[vm.mapdetails[data.id]] = data;
                    } else {
                        vm.mapdetails[data.id] = vm.buyDetail.length;
                        vm.buyDetail.push(data);
                    }
                    vm.pcodigo = '';
                },
                function (er) {
                    vm.pcodigo = '';
                }
            );
        }

        /**
         * Go to orders page
         */
        function gotoOrders() {
            $state.go('app.e-commerce.orders');
        }

        /**
         * Go to product page
         * @param id
         */
        function deleteDetail(id) {
            api.DkPOSWS.BuyDetail
                .delete({ 'idBuy': vm.buy.id, 'id': id },
                    function (data) {
                        vm.buyDetail.splice(vm.mapdetails[id], 1);
                        delete (vm.mapdetails[id]);
                    },
                    function (er) {

                    }
                );
        }

        function changeStatus() {
            api.DkPOSWS.Buy
                .patch({ 'id': vm.buy.id }, {},
                    function (data) {
                        $state.go('app.buy.buy');
                    },
                    function (er) {

                    }
                );
        }
    }
})();