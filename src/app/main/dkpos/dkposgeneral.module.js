(function () {
    'use strict';

    angular
        .module('app.dkposgeneral',
            [
                'app.customer',
                'app.provider',
                'app.product',
                'app.invoice',
                'app.buy',
                'app.auth.register',
                'app.auth.forgot-password'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/dkpos');


    }
})();