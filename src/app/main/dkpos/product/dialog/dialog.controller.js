(function () {
    'use strict';

    angular
        .module('app.product')
        .controller('CreateDetailController', CreateDetailController);

    /** @ngInject */
    function CreateDetailController($mdDialog, api, $timeout, $mdToast, $rootScope, $scope) {
        var vm = this;

        // Data
        vm.product = {};
        vm.loading = true;

        // Methods
        vm.closeDialog = closeDialog;
        vm.saveProduct = saveProduct;

        //////////

        function closeDialog() {
            $mdDialog.cancel();
        }

        /**
        * Save product
        */
        function saveProduct() {
            // Since we have two-way binding in place, we don't really need
            // this function to update the products array in the demo.
            // But in real world, you would need this function to trigger
            // an API call to update your database.
            var promise
                = api.DkPOSWS.Product
                    .save(vm.product).$promise;

            promise.then(
                function (data) {
                    vm.product = data;
                    $mdDialog.hide(vm.product);
                },
                function (er) {

                }
            );
        }


    }
})();
