(function () {
    'use strict';

    angular
        .module('app.product')
        .controller('ProductsController', ProductsController);

    /** @ngInject */
    function ProductsController($state, api) {
        var vm = this;

        // Data

        vm.dtInstance = {};
        vm.dtOptions = {
            dom: 'rt',
            columnDefs: [
                {
                    // Target the id column
                    targets: 0,
                    width: '72px'
                },
                {
                    // Target the image column
                    targets: 1,
                    filterable: false,
                    sortable: false,
                    width: '80px'
                },
                {
                    // Target the quantity column
                    targets: 4,
                    render: function (data, type) {
                        if (type === 'display') {
                            if (parseInt(data) <= 5) {
                                return '<div class="quantity-indicator md-red-500-bg">' + data + '</div>';
                            }
                            else if (parseInt(data) > 5 && parseInt(data) <= 25) {
                                return '<div class="quantity-indicator md-amber-500-bg">' + data + '</div>';
                            }
                            else {
                                return '<div class="quantity-indicator md-green-600-bg">' + data + '</div>';
                            }
                        }

                        return data;
                    }
                },
                {
                    // Target the price column
                    targets: 5,
                    render: function (data, type) {
                        if (type === 'display') {
                            return '<div class="layout-align-start-start layout-row">' + '<span>' + data + '</span>' + '</div>';
                        }

                        return data;
                    }
                },
                {
                    // Target the status column
                    targets: 6,
                    filterable: false,
                    render: function (data, type) {
                        if (type === 'display' && !data.startsWith('<i class')) {
                            if (data === 'true') {
                                return '<i class="icon-checkbox-marked-circle green-500-fg"></i>';
                            }

                            return '<i class="icon-cancel red-500-fg"></i>';
                        }

                        if (type === 'filter') {
                            if (data) {
                                return '1';
                            }

                            return '0';
                        }

                        return data;
                    }
                },
                {
                    // Target the actions column
                    targets: 7,
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false
                }
            ],
            initComplete: function () {
                var api = this.api(),
                    searchBox = angular.element('body').find('#e-commerce-products-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function (event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            paging: false,
            scrollY: 'auto',
            responsive: true
        };

        // Methods
        vm.gotoAddProduct = gotoAddProduct;
        vm.gotoProductDetail = gotoProductDetail;


        init();
        //////////

        function init() {
            api.DkPOSWS.Product.query({}, function (data) {
                vm.products = data;
            }, function (er) {
                vm.products = [];
            });
        }

        /**
         * Go to add product
         */
        function gotoAddProduct() {
            $state.go('app.product.product.edit', { id: 0 });
        }

        /**
         * Go to product detail
         *
         * @param id
         */
        function gotoProductDetail(id) {
            $state.go('app.product.product.edit', { id: id });
        }
    }
})();