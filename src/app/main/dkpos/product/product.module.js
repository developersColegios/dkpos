(function () {
    'use strict';

    angular
        .module('app.product', [
            'wipImageZoom',
            'datatables',
            'textAngular',
            'flow',
            'nvd3',
            'xeditable'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
        .state('app.product', {
                abstract: true,
                url: '/product'
            })
            .state('app.product.product', {
                url: '/',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/product/products.html',
                        controller: 'ProductsController as vm'
                    }
                },
                bodyClass: 'ecommerce'
            })
            .state('app.product.product.edit', {
                url: ':id',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/product/product-edit/product-edit.html',
                        controller: 'ProductEditController as vm'
                    }
                },
                bodyClass: 'e-commerce'
            });


        // Navigation
        msNavigationServiceProvider.saveItem('apps.product', {
            title: 'Productos',
            state: 'app.product.product',
            icon: 'icon-restore',
            weight: 2
        });
    }

})();