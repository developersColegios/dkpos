(function () {
    'use strict';

    angular
        .module('app.product')
        .controller('ProductEditController', ProductEditController);

    /** @ngInject */
    function ProductEditController($scope, api, $state, $stateParams) {
        var vm = this;

        // Data
        vm.product = {};
        vm.ngFlowOptions = {
            // You can configure the ngFlow from here
            /*target                   : 'api/media/image',
             chunkSize                : 15 * 1024 * 1024,
             maxChunkRetries          : 1,
             simultaneousUploads      : 1,
             testChunks               : false,
             progressCallbacksInterval: 1000*/
        };
        vm.ngFlow = {
            // ng-flow will be injected into here through its directive
            flow: {}
        };
        vm.dropping = false;
        vm.imageZoomOptions = {};

        // Methods
        vm.saveProduct = saveProduct;
        vm.gotoProducts = gotoProducts;
        vm.fileAdded = fileAdded;
        vm.upload = upload;
        vm.fileSuccess = fileSuccess;
        vm.isFormValid = isFormValid;
        vm.updateImageZoomOptions = updateImageZoomOptions;

        //////////

        init();

        /**
         * Initialize
         */
        function init() {

            if ($stateParams.id !== 0) {
                api.DkPOSWS.Product.get({ 'id': $stateParams.id }, function (data) {
                    vm.product = data;
                    if (vm.product.pictureRef) {
                        vm.updateImageZoomOptions(vm.product.pictureRef);
                    }
                });
            }
        }

        /**
         * Save product
         */
        function saveProduct() {
            // Since we have two-way binding in place, we don't really need
            // this function to update the products array in the demo.
            // But in real world, you would need this function to trigger
            // an API call to update your database.
            var promise;
            if (vm.product.id) {
                promise = api.DkPOSWS.Product
                    .update({ id: vm.product.id }, vm.product).$promise;
            } else {
                promise = api.DkPOSWS.Product
                    .save(vm.product).$promise;
            }

            promise.then(
                function (data) {
                    vm.product = data;
                    $state.go('app.product.product');
                },
                function (er) {

                }
            );
        }

        /**
         * Go to products page
         */
        function gotoProducts() {
            $state.go('app.product.product');
        }

        /**
         * File added callback
         * Triggers when files added to the uploader
         *
         * @param file
         */
        function fileAdded(file) {
            // Prepare the temp file data for media list
            var uploadingFile = {
                id: file.uniqueIdentifier,
                file: file,
                type: 'uploading'
            };

            // Append it to the media list
            vm.product.images.unshift(uploadingFile);
        }

        /**
         * Upload
         * Automatically triggers when files added to the uploader
         */
        function upload() {
            // Set headers
            vm.ngFlow.flow.opts.headers = {
                'X-Requested-With': 'XMLHttpRequest',
                //'X-XSRF-TOKEN'    : $cookies.get('XSRF-TOKEN')
            };

            vm.ngFlow.flow.upload();
        }

        /**
         * File upload success callback
         * Triggers when single upload completed
         *
         * @param file
         * @param message
         */
        function fileSuccess(file, message) {
            // Iterate through the media list, find the one we
            // are added as a temp and replace its data
            // Normally you would parse the message and extract
            // the uploaded file data from it
            angular.forEach(vm.product.images, function (media, index) {
                if (media.id === file.uniqueIdentifier) {
                    // Normally you would update the media item
                    // from database but we are cheating here!
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(media.file.file);
                    fileReader.onload = function (event) {
                        media.url = event.target.result;
                    };

                    // Update the image type so the overlay can go away
                    media.type = 'image';
                }
            });
        }

        /**
         * Checks if the given form valid
         *
         * @param formName
         */
        function isFormValid(formName) {
            if ($scope[formName] && $scope[formName].$valid) {
                return $scope[formName].$valid;
            }
        }

        /**
         * Update image zoom options
         *
         * @param url
         */
        function updateImageZoomOptions(url) {
            vm.imageZoomOptions = {
                images: [
                    {
                        thumb: url,
                        medium: url,
                        large: url
                    }
                ]
            };
        }
    }
})();