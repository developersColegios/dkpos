(function () {
    'use strict';

    angular
        .module('app.customer')
        .controller('CustomerEditController', CustomerEditController);

    /** @ngInject */
    function CustomerEditController($scope, $state, api, $stateParams) {
        var vm = this;

        // Data
        vm.cliente = {};

        // Methods
        vm.saveCustomer = saveCustomer;
        vm.gotoClients = gotoClients;
        vm.isFormValid = isFormValid;

        //////////

        init();

        /**
         * Initialize
         */
        function init() {
            if ($stateParams.id !== 0) {
                api.DkPOSWS.Customer.get({ 'id': $stateParams.id }, function (data) {
                    vm.cliente = data;
                });
            }
        }

        /**
         * Save product
         */
        function saveCustomer() {
            var promise;
            if (vm.cliente.id) {
                promise = api.DkPOSWS.Customer
                    .update({ id: vm.cliente.id }, vm.cliente).$promise;
            } else {
                promise = api.DkPOSWS.Customer
                    .save(vm.cliente).$promise;
            }

            promise.then(
                function (data) {
                    vm.cliente = data;
                    $state.go('app.customer.customer');
                },
                function (er) {
                    
                }
            );

        }

        /**
         * Go to products page
         */
        function gotoClients() {
            $state.go('app.customer.customer');
        }

        /**
         * Checks if the given form valid
         *
         * @param formName
         */
        function isFormValid(formName) {
            if ($scope[formName] && $scope[formName].$valid) {
                return $scope[formName].$valid;
            }
        }
    }
})();