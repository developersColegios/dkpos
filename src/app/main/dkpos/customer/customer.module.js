(function () {
    'use strict';

    angular
        .module('app.customer', [
            'wipImageZoom',
            'datatables',
            'textAngular',
            'xeditable'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.customer', {
                abstract: true,
                url: '/customer'
            })
            .state('app.customer.customer', {
                url: '/',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/customer/customer.html',
                        controller: 'CustomerController as vm'
                    }
                },
                bodyClass: 'ecommerce'
            })
            .state('app.customer.customer.edit', {
                url: ':id',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/dkpos/customer/customer-edit/customer-edit.html',
                        controller: 'CustomerEditController as vm'
                    }
                },
                bodyClass: 'e-commerce'
            });


        // Navigation
        msNavigationServiceProvider.saveItem('apps.customer', {
            title: 'Clientes',
            state: 'app.customer.customer',
            icon: 'icon-people',
            weight: 2
        });
    }

})();