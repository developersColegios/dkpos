(function () {
    'use strict';

    angular
        .module('app.customer')
        .controller('CustomerController', CustomerController);

    /** @ngInject */
    function CustomerController($state, api, $mdToast) {
        var vm = this;

        // Data
        vm.clientes = [];

        vm.dtInstance = {};
        vm.dtOptions = {
            dom: 'rt',
            columnDefs: [
                {
                    targets: 0,
                    responsivePriority: 4,
                    width: '72px'
                },
                {
                    targets: 1,
                    responsivePriority: 2,
                    width: '80px'
                },
                {
                    targets: 2,
                    responsivePriority: 4,
                    width: '80px'
                },
                {
                    targets: 3,
                    responsivePriority: 3,
                    width: '180px'
                },
                {
                    targets: 4,
                    responsivePriority: 5,
                    width: '80px'
                },
                {
                    targets: 5,
                    responsivePriority: 5,
                    width: '180px'
                },
                {
                    // Target the actions column
                    targets: 6,
                    responsivePriority: 1,
                    filterable: false,
                    sortable: false,
                    width: '80px'
                }
            ],
            initComplete: function () {
                var api = this.api(),
                    searchBox = angular.element('body').find('#e-commerce-products-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0) {
                    searchBox.on('keyup', function (event) {
                        api.search(event.target.value).draw();
                    });
                }
            },
            paging: false,
            scrollY: 'auto',
            responsive: true
        };

        // Methods
        vm.gotoAddCustomer = gotoAddCustomer;
        vm.gotoCustomerDetail = gotoCustomerDetail;
        vm.deleteCustomer = deleteCustomer;

        init();
        //////////

        function init() {
            api.DkPOSWS.Customer.query({}, function (data) {
                vm.clientes = data;
            });
        }

        /**
         * Go to add product
         */
        function gotoAddCustomer() {
            $state.go('app.customer.customer.edit', { id: 0 });
        }

        /**
         * Go to product detail
         *
         * @param id
         */
        function gotoCustomerDetail(id) {
            $state.go('app.customer.customer.edit', { id: id });
        }

        function deleteCustomer(id, i) {
            api.DkPOSWS.Customer
                .delete({ id: id },
                    function (data) {
                        vm.clientes.splice(i, 1);
                        toast('Eliminado exitosamente');
                    },
                    function (er) {

                    }
                );
        }

        function toast(msg) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(msg)
                    .position("top right")
                    .hideDelay(3000)
            );
        }
    }
})();