(function () {
    'use strict';

    angular
        .module('app.dashboards',
            [
                // 3rd Party Dependencies
                'nvd3',
                'datatables'
            ]
        )
        .config(config);

    /** @ngInject */
    function config(msNavigationServiceProvider, $stateProvider, msApiProvider) {
        // State
        $stateProvider.state('app.dashboards_project', {
            url: '/dashboard-project',
            views: {
                'content@app': {
                    templateUrl: 'app/main/dkpos/dashboards/dashboard-project.html',
                    controller: 'DashboardProjectController as vm'
                }
            },
            resolve: {
                DashboardData: function () {
                    return {};
                }
            },
            bodyClass: 'dashboard-project'
        });

        // Api
        msApiProvider.register('dashboard.project', ['app/data/dashboard/project/data.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('apps', {
            title: 'Modulos',
            group: true,
            weight: 1
        });

        msNavigationServiceProvider.saveItem('apps.dashboards', {
            title: 'Dashboards',
            icon: 'icon-tile-four',
            state: 'app.dashboards_project',
            weight: 1
        });
    }

})();